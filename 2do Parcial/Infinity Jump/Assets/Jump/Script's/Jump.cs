﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Jump : MonoBehaviour {
	[SerializeField] float accumulatedForce = 0;//Fuerza acumulada
	private float rndX = 3f;//Distancia Horizontal
	private float rndY = 4f;//Distancia Vertical
	[SerializeField] GameObject escalera;//Objeto Escalera(s)/Peldaño(s)
	[SerializeField] Text count;//Puntos Acumulados
	private int counter = -1;//contador de puntos

	private bool jump;//Variable de Salto
	private Rigidbody rbd2d;//RIGIDBODY

	// Use this for initialization
	void Start () {
		rbd2d = GetComponent<Rigidbody> (); //Fuerza Aplicada
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Space) && !jump) { //Acumular Fuerza para Salto
			accumulatedForce += 10;
		}
		if (Input.GetKeyUp (KeyCode.Space)) { //Comprovación de ResetJump
			jump = true;
			Invoke ("ResetJump", 0.2f);
		}
	}

		void ResetJump(){ //ResetJump dejando en Inicio de Nuevo
			jump = false;
			accumulatedForce = 0;
		}

	void OnTriggerEnter(Collider other){//Aparecer los 'Peldaños'
		if (other.gameObject.CompareTag ("escalon")) {
			Instantiate (escalera, new Vector3 (rndX, rndY, 0), Quaternion.identity);
			rndX = rndX + 5f;//Separacion en posicion X
			rndY = rndY + 4f;//Separacion en posicion Y
		}
		if (other.gameObject.CompareTag ("escalera")) {//Contar los 'Peldaños'
			counter = counter + 1;
			count.text = "Puntos: " + counter.ToString ();
		}
	}

	void OnTriggerExit(Collider other){//Destruye los 'Peldaños' cuando los toca
		Destroy (other.gameObject);
	}

	void SetCountText(){//Cuanta los puntos
		count.text = "Puntos: " + counter.ToString ();
	}

	void FixedUpdate(){
		if (jump){ //Salto Aplicado
			rbd2d.AddForce(new Vector3 (accumulatedForce * 0.3f, accumulatedForce, 0));
		}
	}
}
