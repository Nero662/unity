﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Controller : MonoBehaviour {

    [SerializeField] float speed;
    [SerializeField] public GameObject bala;
    //[SerializeField] float Limit;
    //[SerializeField] UIManager uiManager;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector2.left * Time.deltaTime * speed);
            //transform.Translate(Vector2.left * Time.deltaTime * Limit);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector2.right * Time.deltaTime * speed);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(bala, transform.position, Quaternion.identity);
        }
    }
}