﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager GM;

    public GMTest objectOne;
    public AnotherClass objectTwo;

    void Awake()
    {
        if (GM == null)
        {
            GM = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    /*private void Start()
    {
        StartCoroutine(SpawnEnemies());
    }*/
}