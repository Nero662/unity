﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {
	//VELOCIDAD
    [SerializeField] float speed;
    //[SerializeField] UIManager uiManager01;
    //[SerializeField] UIManager uiManager02;
	//CONTROLADOR PLAYER 01
    [SerializeField] GameObject player1;
	//CONTROLADOR PLAYER 02
    [SerializeField] GameObject player2;

    // Update is called once per frame
    void Update() {
		//MOVIMIETO 
        if (Input.GetKey(KeyCode.W))
        {
            player1.transform.Translate(Vector2.up * Time.deltaTime * speed);
        }
        if (Input.GetKey(KeyCode.S))
        {
            player1.transform.Translate(Vector2.down * Time.deltaTime * speed);
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
           player2.transform.Translate(Vector2.up * Time.deltaTime * speed);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            player2.transform.Translate(Vector2.down * Time.deltaTime * speed);
        }
    }
}
