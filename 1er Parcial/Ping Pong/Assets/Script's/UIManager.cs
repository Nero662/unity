﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    [SerializeField] Text counterUI01;
    int counter01;
    [SerializeField] Text counterUI02;
    int counter02;

    public void UpdateCounter()
    {
        counter01 += 1;
        counterUI01.text = "0 " + counter01;

        counter02 += 1;
        counterUI02.text = "0 " + counter02;
    }

}