﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {
	Rigidbody2D rgdb2d;

	 
	// Use this for initialization
	void Start () {
		//OPERACION PARA QUE LA BOLA SE MUEVA
		rgdb2d = GetComponent<Rigidbody2D> ();
		rgdb2d.velocity = new Vector2 (5f, 5f);
		
	}

	// Update is called once per frame
	void Update () {
		//APARICION RANDOM
		if (this.transform.position.x >= 8f) {
			this.transform.position = new Vector3 (0f, 0f, 0f);
		}
		if (this.transform.position.x <= -8f) {
			this.transform.position = new Vector3 (0f, 0f, 0f);
		}
	}
}
