﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{

    [SerializeField]float speed;
    [SerializeField] UIManager uiManager;

    // Use this for initialization
    void Start()
    {

        
    }


    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector2.up * Time.deltaTime * speed);
            //Funciona para poder mover en una direcion (Time.deltaTime), funciona para igualar la velocidad
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector2.down * Time.deltaTime * speed);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector2.left * Time.deltaTime * speed);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector2.right * Time.deltaTime * speed);
        }


    }

    void OnTriggerEnter2D(Collider2D other)
    {
        uiManager.UpdateCounter();
        Destroy(other.gameObject);
        
        /*if (colisionar.gameObject.CompareTag("Colec"))
        {
            colisionar.gameObject.SetActive(false);
        }*/


    }

}